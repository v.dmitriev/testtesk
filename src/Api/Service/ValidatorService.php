<?php

namespace App\Api\Service;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorService
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($object): ?array
    {
        $errors = $this->validator->validate($object);
        if (\count($errors) > 0) {
            $formattedErrors = [];
            foreach ($errors as $error) {
                $formattedErrors[$error->getPropertyPath()] = $error->getMessage();
            }

            return $formattedErrors;
        }

        return null;
    }
}
