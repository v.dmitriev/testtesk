<?php

namespace App\Api\Action\Url\Query;

use App\Url\Service\CommandService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RedirectToOriginalAction extends Controller
{
    private $urlService;

    public function __construct(CommandService $urlService)
    {
        $this->urlService = $urlService;
    }

    /**
     * @Route(
     *     path="/{hashid}",
     *     methods={"GET"},
     *     name="api_redirect_to_original")
     * @param Request $request
     * @param $hashid
     * @return RedirectResponse
     */
    public function __invoke(Request $request, $hashid)
    {
        $client = $this->get('app.cache.widget');
        if (!$client->hasItem($hashid)) {
            $originalUrl = $this->urlService->redirectToUrl($hashid);
            $cachedItem = $client->getItem($hashid);
            $cachedItem->set($originalUrl);
            $client->save($cachedItem);
        } else {
            $cachedItem = $client->getItem($hashid);
            $originalUrl = $cachedItem->get();
        }

        return new RedirectResponse($originalUrl);
    }
}
