<?php

namespace App\Api\Action\Url\Command;

use App\Api\Exception\ValidationException;
use App\Api\Service\ValidatorService;
use App\Url\Model\DTO\UrlDTO;
use App\Url\Service\CommandService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ShorterUrlAction extends Controller
{
    private $validator;
    private $urlService;

    public function __construct(ValidatorService $validator, CommandService $urlService)
    {
        $this->validator = $validator;
        $this->urlService = $urlService;
    }

    /**
     * @Route(
     *     path="/shorter-url",
     *     methods={"POST"},
     *     name="api_shorter-url")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request)
    {
        $url = $request->get('url');
        $dto = new UrlDTO($url);
        $errors = $this->validator->validate($dto);

        if ($errors) {
            throw new ValidationException($errors);
        }

        try {
            $shortUrl = $this->urlService->shortenUrl($dto);
        } catch (\Exception $exception) {

        }

        return new JsonResponse([
            'url' => $shortUrl,
        ]);
    }
}
