<?php

namespace App\Api\Exception;

use Exception;

class ValidationException extends Exception
{
    private $errors;

    public function __construct(Array $error, Exception $previous = null)
    {
        $this->errors = $error;
        parent::__construct('validation errors', 0, $previous);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}