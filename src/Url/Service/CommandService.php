<?php

namespace App\Url\Service;

use App\Url\Model\DTO\UrlDTO;
use App\Url\Model\Repository\UrlRepository;
use App\Url\Model\Url;
use Roukmoute\HashidsBundle\Hashids;

class CommandService
{
    private $urlRepository;
    private $hashIds;

    public function __construct(UrlRepository $urlRepository, Hashids $hashIds)
    {
        $this->urlRepository = $urlRepository;
        $this->hashIds = $hashIds;
    }

    public function shortenUrl(UrlDTO $urlDTO): string
    {
        $longUrl = $urlDTO->getUrl();
        $numRow = $this->urlRepository->count([]);
        $shortUrl = $_ENV['SITE'].$this->hashIds->encode($numRow);
        $urls = new Url($longUrl, $shortUrl);
        $this->urlRepository->save($urls);

        return $shortUrl;
    }

    public function redirectToUrl($hashid): string
    {
        /**
         * @var Url $url
         */
        $url = $this->urlRepository->getByShortUrl($_ENV['SITE'].$hashid);

        return $url->getLongUrl();
    }
}
