<?php

namespace App\Url\Model;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="urls")
 */
class Url
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $longUrl;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $shortUrl;

    public function __construct($longUrl, $shortUrl)
    {
        $this->id = Uuid::uuid4();
        $this->longUrl = $longUrl;
        $this->shortUrl = $shortUrl;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLongUrl(): string
    {
        return $this->longUrl;
    }

    /**
     * @param mixed $longUrl
     */
    public function setLongUrl($longUrl): void
    {
        $this->longUrl = $longUrl;
    }

    /**
     * @return string
     */
    public function getShortUrl(): string
    {
        return $this->shortUrl;
    }

    /**
     * @param mixed $shortUrl
     */
    public function setShortUrl($shortUrl): void
    {
        $this->shortUrl = $shortUrl;
    }
}
