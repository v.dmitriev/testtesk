<?php

namespace App\Url\Model\Exception;

use Exception;

class NotFoundException extends Exception
{
    public function getMessageKey(): string
    {
        return 'url.model.url.not_found';
    }
}