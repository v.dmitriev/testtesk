<?php

namespace App\Url\Model\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class UrlDTO
{
    /**
     * @Assert\NotBlank()
     * @Assert\Url(
     *     message = "The url '{{ value }}' is not a valid url",
     * )
     */
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }
}
