<?php

namespace App\Url\Model\Repository;

use App\Url\Model\Exception\NotFoundException;
use App\Url\Model\Url;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class UrlRepository extends ServiceEntityRepository
{
    private $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct($registry, Url::class);
    }

    public function save(Url $url): void
    {
        $this->em->persist($url);
        $this->em->flush();
    }

    /**
     * @param $shortUrl
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function getByShortUrl($shortUrl)
    {
        $result = $this->em->createQueryBuilder()
            ->select('u')
            ->from(Url::class, 'u')
            ->where('u.shortUrl = :shortUrl')
            ->setParameter('shortUrl', $shortUrl)
            ->getQuery()->getOneOrNullResult();

        if (empty($result)) {
            throw new NotFoundException('Url with short url: ' . $shortUrl . ' not found');
        }

        return $result;
    }
}
