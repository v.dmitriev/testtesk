# Тестовое задание.
`Описание:`
Написать простой сокращатель ссылок и gate для редиректа.
`Функциональные требования к ужиматору:`
— на вход подается ссылка, которую нужно ужать;
— на выход нужно получить сокращенную ссылку с длиной параметров урла <=6 символов;
`Функциональные требования к гейту редиректа:`
— при заходе на гейт должен происходить редирект на начальную ссылку.
`Требования к реализации:`
— ужиматор должен принимать/возвращать данные в json формате;
— для ужимания желательно использовать какую-нибудь либу с гитхаба, подключенную через композер;
— для хранения мапы полная/краткая ссылка использовать любую реляционную СУБД
— запросы на гейт должны кешироваться с TTL 3600 секунд, для кеша использовать redis.

# Команды Docker Compose
  * Стартовать (в фоне): `docker-compose up -d`
  * Стартовать: `docker-compose up`
  * Остановить: `docker-compose stop`
  * Убить: `docker-compose kill`
  * Логи : `docker-compose logs`
  * Выполнить команду внутри: `docker-compose exec SERVICE_NAME COMMAND` где `COMMAND` то что хочешь выполнить
  
    * Bash внутри контейнера, `docker-compose exec php-fpm bash`
    * Symfony console, `docker-compose exec php-fpm bin/console`
    * Mysql, `docker-compose exec mysql mysql -uroot -pCHOSEN_ROOT_PASSWORD`

# Команды Docker    
  * Удалить контейнеры: `docker rm -f $(docker ps -a -q)`
  * Удалить образы: `docker rmi -f $(docker images -a -q)`
  * Удалить volumes: `docker volume rm $(docker volume ls -q)`
  * Удалить networks: `docker network rm $(docker network ls | tail -n+2 | awk '{if($2 !~ /bridge|none|host/){ print 
 $1 }}')`
